<link href="../../jsp/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="../../jsp/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:500,900italic,900,400italic,100,700italic,300,700,500italic,100italic,300italic,400' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>
<!-- js -->
<script src="../../jsp/js/jquery.min.js"></script>

<script src="../../jsp/js/chili-1.8b.js"></script>
<script src="../../jsp/js/jquery-latest.js"></script>
<script src="../../jsp/js/jquery.tablesorter.js"></script>
<script src="../../jsp/js/docs.js"></script>


<%--<%@include file="js/jquery-latest.js"%>
<%@include file="js/jquery.tablesorter.js"%>
<%@include file="js/chili-1.8b.js"%>
<%@include file="js/docs.js"%>--%>
<!-- //js -->
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="../../jsp/js/move-top.js"></script>
<script type="text/javascript" src="../../jsp/js/easing.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".scroll").click(function(event){
      event.preventDefault();
      $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
    });
  });
</script>
<!-- start-smoth-scrolling -->
<!--animated-css-->
<link href="../../jsp/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="../../jsp/js/wow.min.js"></script>
<script>
  new WOW().init();
</script>
<!--/animated-css-->

