<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>
<html>
<head>
  <title>Registration</title>
  <%@include file="../script.jsp" %>
</head>
<body>
<!-- header -->
<div class="header-nav">
  <div class="container">
    <div class="logo wow fadeInLeft" data-wow-delay="0.5s">
      <form action="/controller" method="post">
        <input type="image" src="../../jsp/images/logo2.png" onclick="submit()">
        <input type="hidden" name="command" value="TO_PAGE">
        <input type="hidden" name="forwardPage" value="jsp/entrance/authorization.jsp">
      </form>
    </div>
    <div class="logo-right">
      <span class="menu"><img src="../images/menu.png" alt=" "/></span>
      <ul class="nav1">
        <li>
          <form action="/controller" method="post">
            <input type="hidden" name="command" value="CHANGE_LOCALE">
            <input type="hidden" name="forwardPage" value="jsp/entrance/registration.jsp">
            <select name="locale" onchange="submit()">
              <option value="en" ${sessionScope.locale == 'en' ? 'selected' : ''}><fmt:message key="en"/></option>
              <option value="ru" ${sessionScope.locale == 'ru' ? 'selected' : ''}><fmt:message key="rus"/></option>
            </select>
          </form>
        </li>
      </ul>
    </div>
  </div>
</div>

<div class="sign-up-form wow fadeInUp" data-wow-delay="0.5s">
<div class="container">
  <h3><fmt:message key="registration"/> </h3>
  <p><fmt:message key="register_content"/></p>
  <div class="sign-up">
    <form action="/controller" method="post">
      <h6><fmt:message key="login_infomation"/> </h6>
      <div class="sign-u">
        <div class="sign-up1">
          <h4><fmt:message key="Username"/> :</h4>
        </div>
        <div class="sign-up2">
          <input type="text" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$" name="login" placeholder="<fmt:message key="Username"/> " required=" "/>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="sign-u">
        <div class="sign-up1">
          <h4><fmt:message key="password"/>:</h4>
        </div>
        <div class="sign-up2">
          <input type="password" name="password" placeholder="<fmt:message key="password"/>" required=" "/>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="sign-u">
        <div class="sign-up1">
          <h4><fmt:message key="email"/> :</h4>
        </div>
        <div class="sign-up2">
          <input type="email" name="email" placeholder="<fmt:message key="email"/>" required=" "/>
        </div>
        <div class="clearfix"> </div>
      </div>
      <input type="hidden" name="command" value="REGISTRATION">
      <input type="submit" value="<fmt:message key="registration"/> ">
    </form>
  </div>
</div>
</div>
<tag:footerLayout isFixed="true"/>
</body>
</html>
