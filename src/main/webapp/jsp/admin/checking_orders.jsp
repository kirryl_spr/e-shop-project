<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>
<html>
<head>
    <title>Checking orders by admin</title>
    <%@include file="../script.jsp" %>
</head>

<body>

<%@include file="../header.jsp"%>

<div class="single">
    <div class="container">
        <div class="bottom">
            <h1 align="center"><fmt:message key="checking_orders"/></h1>
                <div id="main">
                    <tag:paginalout collection="${orderList}" var="order" >
          <jsp:attribute name="header">
          <table  id="tablesorter" class="tablesorter" border="0" cellpadding="0" cellspacing="1">
            <thead>
            <tr bgcolor="#F5F5F5">
                <th rowspan="2"><fmt:message key="idOrders"/></th>
                <th rowspan="2"><fmt:message key="Username"/></th>
                <th rowspan="2"><fmt:message key="name"/></th>
                <th rowspan="2"><fmt:message key="amount"/></th>
                <th rowspan="2"><fmt:message key="price"/></th>
                <th rowspan="2"><fmt:message key="date"/></th>
                <th rowspan="2"><fmt:message key="payment"/></th>
            </tr>
            </thead>
            </jsp:attribute>
            <jsp:attribute name="footer">
          </table>
          </jsp:attribute>
                        <jsp:body>
                            <tr>
                                <td>
                                        ${order.idOrders}
                                </td>
                                <td>
                                        ${order.login}
                                </td>
                                <td>
                                        ${order.product}
                                </td>
                                <td>
                                        ${order.amount}
                                </td>
                                <td>
                                        ${order.price}
                                </td>
                                <td>
                                        ${order.dateOfOrder}
                                </td>
                                <td>
                                        ${order.paid}
                                </td>
                            </tr>
                        </jsp:body>
                    </tag:paginalout>

                    <%--<table id="tablesorter" class="tablesorter" border="0" cellpadding="0" cellspacing="1">
                        <thead>
                        <tr bgcolor="#F5F5F5">
                            <th rowspan="2"><fmt:message key="idOrders"/></th>
                            <th rowspan="2"><fmt:message key="Username"/></th>
                            <th rowspan="2"><fmt:message key="name"/></th>
                            <th rowspan="2"><fmt:message key="amount"/></th>
                            <th rowspan="2"><fmt:message key="price"/></th>
                            <th rowspan="2"><fmt:message key="date"/></th>
                            <th rowspan="2"><fmt:message key="payment"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="order" items="${ orderList }">
                            <tr>
                                <td>
                                        ${order.idOrders}
                                </td>
                                <td>
                                        ${order.login}
                                </td>
                                <td>
                                        ${order.product}
                                </td>
                                <td>
                                        ${order.amount}
                                </td>
                                <td>
                                        ${order.price}
                                </td>
                                <td>
                                        ${order.dateOfOrder}
                                </td>
                                <td>
                                        ${order.paid}
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>--%>
                    <form action="/controller" method="post" align="center">
                        <input type="hidden" name="command" value="TO_PAGE">
                        <input type="hidden" name="forwardPage" value="jsp/admin/admin_menu.jsp">
                        <input type="submit" class="sbmt-btn" value="<fmt:message key="back"/>">
                    </form>
                </div>
        </div>
    </div>
</div>

<tag:footerLayout isFixed="false"/>

</body>
</html>