<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>
<!DOCTYPE html>
<html>
<head>
  <title>Editing car</title>
  <%@include file="../script.jsp" %>
</head>

<body>

<%@include file="../header.jsp"%>

<div class="single">
  <div class="container">
    <div class="bottom">
      <h1 class="h1-white"><fmt:message key="edit_products"/></h1>
      <form action="/controller" method="post" align="center">
        <table class="table-add">
          <tr>
            <td><fmt:message key="category"/>:</td>
            <td>
              <select name="category" required>
                <option value=""><fmt:message key="choose_category"/></option>
                <c:forEach var="category" items="${categoryList}">
                  <option value="${category.category}">${category.category}</option>
                </c:forEach>
              </select>
            </td>
          </tr>
          <tr>
            <td><fmt:message key="name"/>:</td>
            <td>
              <input type="text" name="name" required>
            </td>
          </tr>
          <tr>
            <td><fmt:message key="price"/>:</td>
            <td>
              <input type="number" name="price" min="1" required>
            </td>
          </tr>
          <tr>
            <td><fmt:message key="amount"/>:</td>
            <td>
              <input type="number" name="amount" min="1" required>
            </td>
          </tr>
        </table>
        <input type="hidden" name="command" value="PERFORM_EDITION">
        <input type="hidden" name="idProduct" value="${idProduct}">
        <input type="submit" class="sbmt-btn" value="<fmt:message key="perform_btn"/>">
      </form>
    </div>
  </div>
</div>

<tag:footerLayout isFixed="true"/>

</body>
</html>