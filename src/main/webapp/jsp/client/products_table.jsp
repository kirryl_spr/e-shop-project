<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>

<html>
<head>
  <title>Catalog</title>
  <%@include file="../script.jsp" %>
</head>

<body>

<%@include file="../header.jsp"%>

<div class="single">
  <div class="container">
    <div class="bottom">
      <h1 align="center"><fmt:message key="see_our_catalog"/></h1>
      <div id="main">

<%--        --%>

        <table id="tablesorter" class="tablesorter" border="0" cellpadding="0" cellspacing="1">
          <thead>
          <tr bgcolor="#F5F5F5">
            <th rowspan="2"><fmt:message key="idProduct"/></th>
            <th rowspan="2"><fmt:message key="name"/></th>
            <th rowspan="2"><fmt:message key="category"/></th>
            <th rowspan="2"><fmt:message key="price"/></th>
            <th rowspan="2"><fmt:message key="amount"/></th>
            <th rowspan="2"></th>
          </tr>
          </thead>
          <tbody id="flags">

          <c:forEach var="product" items="${ productList }">
            <tr>
              <form action="/controller" method="post">
                <td>
                  <input type="hidden" name="idProduct" value="${product.idProduct}">${product.idProduct}
                </td>
                <td>
                  <input type="hidden" name="name" value="${product.name}">${product.name}
                </td>
                <td>
                    ${product.category}
                </td>
                <td>
                  <input type="hidden" name="price" value="${product.price}">${product.price}
                </td>
                <c:choose>
                  <c:when test="${sessionScope.user.blacklist eq true}">
                    <td>
                      <fmt:message key="no_rights"/>
                    </td>
                    <td>
                      <fmt:message key="no_rights"/>
                    </td>
                  </c:when>
                  <c:otherwise>
                    <td>
                      <c:choose>
                        <c:when test="${product.amount eq 0}">
                          <fmt:message key="not_in_stock"/>
                        </c:when>
                        <c:otherwise>
                          <select name="amount" required>
                            <option value=""><fmt:message key="choose_amount"/></option>
                            <c:forEach var="amount" begin="1" end="${product.amount}">
                              <option value="${amount}">${amount}</option>
                            </c:forEach>
                          </select>
                        </c:otherwise>
                      </c:choose>
                    </td>
                    <td>
                      <c:choose>
                        <c:when test="${product.amount eq 0}">
                          :)
                        </c:when>
                        <c:otherwise>
                          <input type="hidden" name="command" value="MAKING_ORDER">
                          <input type="submit" class="sbmt-btn" value="<fmt:message key="order_btn"/>">
                        </c:otherwise>
                      </c:choose>
                    </td>
                  </c:otherwise>
                </c:choose>
              </form>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
      <form action="/controller" method="post" align="center">
        <input type="hidden" name="command" value="TO_PAGE">
        <input type="hidden" name="forwardPage" value="jsp/client/client_menu.jsp">
        <input type="submit" class="sbmt-btn" value="<fmt:message key="back"/>">
      </form>
    </div>
  </div>
</div>
<tag:footerLayout isFixed="false"/>
</body>
</html>