<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>

<!DOCTYPE html>
<html>
<head>
  <title>Error</title>
  <%@include file="script.jsp" %>
</head>

<body>
<div class="header-nav">
  <div class="container">
    <div class="logo wow fadeInLeft" data-wow-delay="0.5s">
      <form action="/controller" method="post">
        <input type="image" src="../../jsp/images/logo2.png" onclick="submit()">
        <input type="hidden" name="command" value="TO_PAGE">
        <input type="hidden" name="forwardPage" value="jsp/entrance/authorization.jsp">
      </form>
    </div>
  </div>
</div>
<div class="single">
  <div class="container">
    <div class="bottom">
      <div id="main">
        <h3 align="center"><fmt:message key="something_wrong"/></h3>
        <p align="center"><fmt:message key="${message}"/></p>
      </div>
    </div>
  </div>
</div>

<div class="fixed-footer">
  <div class="container">
    <p><fmt:message key="footer_content"/></p>
  </div>
</div>

</body>
</html>
