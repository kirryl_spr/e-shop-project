<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:choose>
  <c:when test="${not empty sessionScope.user and sessionScope.user.admin}">
    <div class="header-nav">
      <div class="container">
        <div class="logo wow fadeInLeft" data-wow-delay="0.5s">
          <form action="/controller" method="post">
            <input type="image" src="../../jsp/images/logo2.png" onclick="submit()">
            <input type="hidden" name="command" value="TO_PAGE">
            <input type="hidden" name="forwardPage" value="jsp/admin/admin_menu.jsp">
          </form>
        </div>
        <div class="logo-right">
          <span class="menu"><img src="../images/menu.png" alt=" "/></span>
          <ul class="nav1">
            <li>
              <form action="/controller" method="post" align="center">
                <input type="hidden" name="command" value="ADDING_PRODUCT">
                <input class="menu-btn" type="submit" value="<fmt:message key="add_product"/>">
              </form>
            </li>
            <li>
              <form action="/controller" method="post" align="center">
                <input type="hidden" name="command" value="CHECKING_ORDERS">
                <input class="menu-btn" type="submit" value="<fmt:message key="check_orders"/>">
              </form>
            </li>
            <li>
              <form action="/controller" method="post" align="center">
                <input type="hidden" name="command" value="CHECKING_USERS">
                <input class="menu-btn" type="submit" value="<fmt:message key="check_users"/>">
              </form>
            </li>
            <li>
              <form action="/controller" method="post" align="center">
                <input type="hidden" name="command" value="DELETING_PRODUCT">
                <input class="menu-btn" type="submit" value="<fmt:message key="delete_product"/>">
              </form>
            </li>
            <li>
              <form action="/controller" method="post" align="center">
                <input type="hidden" name="command" value="EDITING_PRODUCT">
                <input class="menu-btn" type="submit" value="<fmt:message key="edit_product"/>">
              </form>
            </li>
            <li>
              <form action="/controller" class="header-form" method="post" >
                <c:if test="${not empty sessionScope.user and not empty sessionScope.user.login}">
                  <fmt:message key="hello"/><c:out value="${sessionScope.user.login}"/>
                </c:if>
                <input type="hidden" name="command" value="SIGN_OUT">
                <input class="menu-logout-btn" type="submit" value="<fmt:message key="sign_out"/>">
              </form>
            </li>
            <li>
              <form action="/controller" method="post">
                <input type="hidden" name="command" value="CHANGE_LOCALE">
                <input type="hidden" name="forwardPage" value="jsp/admin/admin_menu.jsp">
                <select name="locale" onchange="submit()">
                  <option value="en" ${sessionScope.locale == 'en' ? 'selected' : ''}><fmt:message key="en"/></option>
                  <option value="ru" ${sessionScope.locale == 'ru' ? 'selected' : ''}><fmt:message key="rus"/></option>
                </select>
              </form>
            </li>
          </ul>
        </div>
        <!-- script for menu -->
        <script>
          $( "span.menu" ).click(function() {
            $( "ul.nav1" ).slideToggle( 300, function() {
              // Animation complete.
            });
          });
        </script>
        <!-- //script for menu -->
      </div>
    </div>

  </c:when>
  <c:otherwise>
    <div class="header-nav">
      <div class="container">
        <div class="logo wow fadeInLeft" data-wow-delay="0.5s">
          <form action="/controller" method="post">
            <input type="image" src="../../jsp/images/logo2.png" onclick="submit()">
            <input type="hidden" name="command" value="TO_PAGE">
            <input type="hidden" name="forwardPage" value="jsp/client/client_menu.jsp">
          </form>
        </div>
        <div class="logo-right">
          <span class="menu"><img src="../images/menu.png" alt=" "/></span>
          <ul class="nav1">
            <li>
              <form action="/controller" method="post" align="center">
                <input type="hidden" name="command" value="LOOKING_PRODUCTS">
                <input type="submit" class="menu-btn"  value="<fmt:message key="catalog_of_products"/>">
              </form>
            </li>
            <li>
              <form action="/controller" method="post" align="center">
                <input type="hidden" name="command" value="LOOKING_ORDERS">
                <input  class="menu-btn" type="submit" value="<fmt:message key="check_my_orders"/>">
              </form>
            </li>
            <li>
              <form class="header-form" method="post" action="/controller">
                <c:if test="${not empty sessionScope.user and not empty sessionScope.user.login}">
                  <fmt:message key="hello"/> <c:out value="${sessionScope.user.login}"/>
                </c:if>
                <input type="hidden" name="command" value="SIGN_OUT">
                <input class="menu-logout-btn" type="submit" value="<fmt:message key="sign_out"/>">
              </form>
            </li>
            <li>
              <form action="/controller" method="post">
                <input type="hidden" name="command" value="CHANGE_LOCALE">
                <input type="hidden" name="forwardPage" value="jsp/client/client_menu.jsp">
                <select name="locale" onchange="submit()">
                  <option value="en" ${sessionScope.locale == 'en' ? 'selected' : ''}><fmt:message key="en"/></option>
                  <option value="ru" ${sessionScope.locale == 'ru' ? 'selected' : ''}><fmt:message key="rus"/></option>
                </select>
              </form>
            </li>
          </ul>
        </div>
        <!-- script for menu -->
        <script>
          $( "span.menu" ).click(function() {
            $( "ul.nav1" ).slideToggle( 300, function() {
              // Animation complete.
            });
          });
        </script>
        <!-- //script for menu -->
      </div>
    </div>
  </c:otherwise>
</c:choose>




