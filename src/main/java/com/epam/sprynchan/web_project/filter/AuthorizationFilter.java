package com.epam.sprynchan.web_project.filter;


import com.epam.sprynchan.web_project.command.CommandEnum;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.model.domain.User;
import com.epam.sprynchan.web_project.resource.Resource;
import com.epam.sprynchan.web_project.util.UserTypeEnum;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Class of authorization filter
 *
 * @author Kirill
 */
public class AuthorizationFilter implements Filter {
    /**
     * Key to bundle of error - page
     */
    private static final String FORWARD_ERROR = "forward.error";
    /**
     * Parameters for working with request
     */
    public static final String PARAMETER_USER = "user";
    public static final String PARAMETER_MESSAGE = "message";
    public static final String PARAMETER_COMMAND = "command";
    /**
     * Keys to bundle for different error messages
     */
    private static final String MESSAGE_NO_RIGHTS = "no_rights_message";
    private static final String ERROR_IN_GETTING_COMMAND = "error.in.getting.command";
    private static final String ERROR_IN_CREATING_COMMAND = "error.in.creating.command";

    /**
     * Logger that makes some notes at log
     */
    public static final Logger logger = Logger.getLogger(AuthorizationFilter.class);
    /**
     * Initializing method
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * Method that making filtration
     *
     * @param request
     * @param response
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession(true);

        User user = (User) session.getAttribute(PARAMETER_USER);

        try {
            String command = request.getParameter(PARAMETER_COMMAND);

            CommandEnum commandType = getCommandEnum(command);

            boolean authorized = checkAccess(user, commandType);

            if (authorized) {
                filterChain.doFilter(request, response);
            } else {
                request.setAttribute(PARAMETER_MESSAGE, MESSAGE_NO_RIGHTS);
                request.getRequestDispatcher(Resource.getStr(FORWARD_ERROR)).forward(request, response);
            }
        } catch (CommandException e) {
            logger.error(e.getMessage());
        }

    }

    /**
     * Destroying method
     */
    @Override
    public void destroy() {

    }

    /**
     * The main method of filter logic.
     * Checks the access for different commands.
     *
     * @param user
     * @param command
     * @return
     */
    private boolean checkAccess(User user, CommandEnum command) {
        boolean permission = false;

        if (user != null) {
            boolean userStatus = user.isAdmin();

            if (userStatus && command.getUserType() == UserTypeEnum.ADMIN) {
                permission = true;
            } else if (!userStatus && command.getUserType() == UserTypeEnum.USER) {
                permission = true;
            } else if (command.getUserType() == UserTypeEnum.ALL) {
                permission = true;
            } else {
                permission = false;
            }
        } else if (command.getUserType() == UserTypeEnum.ALL) {
            permission = true;
        }
        return permission;
    }

    /**
     * Method which takes a command from enum of commands
     *
     * @param command
     * @return CommandEnum
     * @throws CommandException
     */
    private static CommandEnum getCommandEnum(String command) throws CommandException {
        CommandEnum commandEnum;

        try {
            commandEnum = CommandEnum.valueOf(command);
        } catch (IllegalArgumentException ex) {
            throw new CommandException(ERROR_IN_GETTING_COMMAND, ex);
        } catch (NullPointerException ex) {
            throw new CommandException(ERROR_IN_CREATING_COMMAND, ex);
        }
        return commandEnum;
    }
}
