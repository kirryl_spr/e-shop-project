package com.epam.sprynchan.web_project.command.entrance;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class implements a pattern command
 * This class that signs out user from syste
 * @author Kirill
 */

public class SignOut extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_AUTHORIZATION = "forward.autho";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_USER = "user";

    /**
     * Method that signs out user from system
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        request.getSession().setAttribute(PARAMETER_USER, null);
        setForward(Resource.getStr(FORWARD_AUTHORIZATION));
    }
}

