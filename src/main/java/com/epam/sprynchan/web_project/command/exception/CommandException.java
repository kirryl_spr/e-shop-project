package com.epam.sprynchan.web_project.command.exception;

/**
 * @author Kirill
 *         <code>CommandException</code>
 *         <p/>
 *         Exception class created specifically to describe the exceptional
 *         situation arises in the Command Factory layer application.
 * @see java.lang.Exception
 */
public class CommandException extends Exception {
    public CommandException() {
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }

    public CommandException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
