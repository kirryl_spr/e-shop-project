package com.epam.sprynchan.web_project.command.admin;


import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.UserDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.User;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that shows users from database to admin
 * @author Kirill
 */

public class CheckingUsers extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_CHECKING_USERS = "forward.checking.users";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_USERS_LIST = "userList";

    /**
     * Method that shows users from database to admin
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        UserDao userDao = new UserDao();
        List<User> userList;

        try {
            userList = userDao.buildList();

            request.setAttribute(PARAMETER_USERS_LIST, userList);
            setForward(Resource.getStr(FORWARD_CHECKING_USERS));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
