package com.epam.sprynchan.web_project.command.entrance;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.UserDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.User;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class implements a pattern command
 * This class that authorize users in system
 * @author Kirill
 */

public class Authorization extends Command {

    /**
     * Keys to bundle
     */
    public static final String FORWARD_AUTHORIZATION = "forward.autho";
    public static final String FORWARD_CLIENT_MENU = "forward.client.menu";
    public static final String FORWARD_ADMIN_MENU = "forward.admin.menu";

    /**
     * Parameters for working with request
     */
    public static final String PARAMETER_LOGIN = "login";
    public static final String PARAMETER_PASSWORD = "password";
    public static final String PARAMETER_USER = "user";
    public static final String PARAMETER_MESSAGE = "message";

    /**
     * Key for no such user message
     */
    public static final String MESSAGE = "no_such_user_message";

    /**
     * Method that authorize users in system
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        User user;
        UserDao userDao = new UserDao();

        String login = request.getParameter(PARAMETER_LOGIN);
        String password = request.getParameter(PARAMETER_PASSWORD);

        try {
            user = userDao.getUserByLoginPassword(login, password);

            if (user != null) {

                if (user.isAdmin()) {
                    setForward(Resource.getStr(FORWARD_ADMIN_MENU));
                    request.getSession().setAttribute(PARAMETER_USER, user);
                } else {
                    setForward(Resource.getStr(FORWARD_CLIENT_MENU));
                    request.getSession().setAttribute(PARAMETER_USER, user);
                }

            } else {
                request.getSession().setAttribute(PARAMETER_USER, null);
                request.setAttribute(PARAMETER_MESSAGE, Resource.getStr(MESSAGE));
                setForward(Resource.getStr(FORWARD_AUTHORIZATION));
            }

        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
