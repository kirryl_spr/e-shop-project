package com.epam.sprynchan.web_project.command.entrance;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.UserDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.builders.UserBuilder;
import com.epam.sprynchan.web_project.model.domain.User;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class implements a pattern command
 * This class that register user in system
 * @author Kirill
 */

public class Registration extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_AUTHORIZATION = "forward.autho";

    /**
     * Parameters for working with request
     */
    private static final String ERROR_IN_BUILDING_USER = "error.in.building.user";

    /**
     * Method that register user in system
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        UserDao userDao = new UserDao();

        try {
            UserBuilder userBuilder = new UserBuilder();

            if (userBuilder.build(request)) {
                User user = userBuilder.getUser();

                userDao.insertUser(user);

                setForward(Resource.getStr(FORWARD_AUTHORIZATION));
            } else {
                throw new CommandException(ERROR_IN_BUILDING_USER);
            }

        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
