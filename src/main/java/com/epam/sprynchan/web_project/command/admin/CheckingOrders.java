package com.epam.sprynchan.web_project.command.admin;


import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.OrderDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Order;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that shows orders from database to admin
 * @author Kirill
 */

public class CheckingOrders extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_CHECKING_ORDERS = "forward.check.orders";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_ORDERS_LIST = "orderList";

    /**
     * Method that shows users from database to admin
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        OrderDao orderDao = new OrderDao();
        List<Order> orderList;

        try {
            orderList = orderDao.buildList();

            request.setAttribute(PARAMETER_ORDERS_LIST, orderList);
            setForward(Resource.getStr(FORWARD_CHECKING_ORDERS));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
