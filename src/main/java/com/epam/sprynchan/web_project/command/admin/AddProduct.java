package com.epam.sprynchan.web_project.command.admin;


import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.ProductDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class implements a pattern command
 * This class  adds product to database
 * @author Kirill
 */
public class AddProduct extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_ADD_PRODUCT = "forward.adding.product";
    private static final String DONE_MESSAGE = "done_product_message";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_NAME = "name";
    private static final String PARAMETER_CATEGORY = "category";
    private static final String PARAMETER_PRICE = "price";
    private static final String PARAMETER_AMOUNT = "amount";
    private static final String PARAMETER_DONE = "DONE";


    /**
     * Method adds product to database
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        ProductDao productDao = new ProductDao();

        try {
            String name = request.getParameter(PARAMETER_NAME);
            String category = request.getParameter(PARAMETER_CATEGORY);
            int price = Integer.parseInt(request.getParameter(PARAMETER_PRICE));
            int amount = Integer.parseInt(request.getParameter(PARAMETER_AMOUNT));

            productDao.addProduct(name, category, price, amount);

            request.setAttribute(PARAMETER_DONE, Resource.getStr(DONE_MESSAGE));
            setForward(Resource.getStr(FORWARD_ADD_PRODUCT));

        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
