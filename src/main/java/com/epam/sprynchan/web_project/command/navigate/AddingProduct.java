package com.epam.sprynchan.web_project.command.navigate;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.CategoryDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Category;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that forward user to adding product page and loads the list of categories
 * @author Kirill
 */

public class AddingProduct extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_ADDING_PRODUCT = "forward.adding.product";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_CATEGORIES_LIST = "categoryList";

    /**
     * Method that forward user to adding product page and loads the list of categories
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        CategoryDao categoryDAO = new CategoryDao();
        List<Category> categoryList;

        try {
            categoryList = categoryDAO.buildList();

            request.setAttribute(PARAMETER_CATEGORIES_LIST, categoryList);
            setForward(Resource.getStr(FORWARD_ADDING_PRODUCT));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
