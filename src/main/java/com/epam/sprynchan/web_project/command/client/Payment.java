package com.epam.sprynchan.web_project.command.client;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.OrderDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Order;
import com.epam.sprynchan.web_project.model.domain.User;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that makes payment of concrete order of user
 * @author Kirill
 */

public class Payment extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_ORDERS_USER = "forward.orders_user";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_ID_ORDERS = "idOrders";
    private static final String PARAMETER_USER = "user";
    private static final String PARAMETER_ORDERS_LIST = "orderList";

    /**
     * Method that makes payment of concrete order of user
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        OrderDao orderDao = new OrderDao();
        List<Order> orderList;

        try {
            int idOrder = Integer.parseInt(request.getParameter(PARAMETER_ID_ORDERS));

            orderDao.payForOrder(idOrder);

            orderList = orderDao.buildListOfOrders((User) request.getSession().getAttribute(PARAMETER_USER));
            request.setAttribute(PARAMETER_ORDERS_LIST, orderList);
            setForward(Resource.getStr(FORWARD_ORDERS_USER));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
