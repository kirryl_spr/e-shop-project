package com.epam.sprynchan.web_project.command;

import com.epam.sprynchan.web_project.util.UserTypeEnum;

/**
 * This enum contains enumeration of all commands
 *
 * @author Kirill
 */
public enum CommandEnum {
    REGISTRATION(UserTypeEnum.ALL),
    TO_PAGE(UserTypeEnum.ALL),
    AUTHORIZATION(UserTypeEnum.ALL),
    LOOKING_PRODUCTS(UserTypeEnum.ALL),
    SIGN_OUT(UserTypeEnum.ALL),
    CHANGE_LOCALE(UserTypeEnum.ALL),

    MAKING_ORDER(UserTypeEnum.USER),
    LOOKING_ORDERS(UserTypeEnum.USER),
    PAYMENT(UserTypeEnum.USER),

    ADDING_PRODUCT(UserTypeEnum.ADMIN),
    ADD_PRODUCT(UserTypeEnum.ADMIN),
    DELETING_PRODUCT(UserTypeEnum.ADMIN),
    DELETE_PRODUCT(UserTypeEnum.ADMIN),
    EDITING_PRODUCT(UserTypeEnum.ADMIN),
    EDIT_PRODUCT(UserTypeEnum.ADMIN),
    PERFORM_EDITION(UserTypeEnum.ADMIN),
    CHECKING_ORDERS(UserTypeEnum.ADMIN),
    CHECKING_USERS(UserTypeEnum.ADMIN),
    IN_BLACKLIST(UserTypeEnum.ADMIN),
    OUT_BLACKLIST(UserTypeEnum.ADMIN);

    /**
     * This is instance which defines what users can execute this command
     */
    private UserTypeEnum userType;

    private CommandEnum(UserTypeEnum userType) {
        this.userType = userType;
    }

    public UserTypeEnum getUserType() {
        return userType;
    }
}
