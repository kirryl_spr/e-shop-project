package com.epam.sprynchan.web_project.command.admin;


import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.CategoryDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Category;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that edit product in database
 * @author Kirill
 */

public class EditProduct extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_EDIT_PRODUCT_BY_ID = "forward.edit.product.by.id";


    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_ID_PRODUCT = "idProduct";
    private static final String PARAMETER_CATEGORIES_LIST = "categoryList";

    /**
     * Method that edit product in database
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        CategoryDao categoryDAO = new CategoryDao();
        List<Category> categoryList;

        try {
            int idProduct = Integer.parseInt(request.getParameter(PARAMETER_ID_PRODUCT));

            categoryList = categoryDAO.buildList();

            request.setAttribute(PARAMETER_CATEGORIES_LIST, categoryList);
            request.setAttribute(PARAMETER_ID_PRODUCT, idProduct);
            setForward(Resource.getStr(FORWARD_EDIT_PRODUCT_BY_ID));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
