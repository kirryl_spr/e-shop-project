package com.epam.sprynchan.web_project.command.navigate;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class implements a pattern command
 * This class that forward user to necessary page
 * @author Kirill
 */

public class ToPage extends Command {

    /**
     * Parameters for working with request
     */
    private static final String ERROR_GOING_TO_ANOTHER_PAGE = "error.to.page";
    private static final String PARAMETER_PAGE = "forwardPage";

    /**
     * Method that forward user to necessary page
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String forward = request.getParameter(PARAMETER_PAGE);

        if (forward != null) {
            setForward(forward);
        } else {
            throw new CommandException(ERROR_GOING_TO_ANOTHER_PAGE);
        }
    }

}
