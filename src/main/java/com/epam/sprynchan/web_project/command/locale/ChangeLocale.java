package com.epam.sprynchan.web_project.command.locale;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.OrderDao;
import com.epam.sprynchan.web_project.dao.implementation.ProductDao;
import com.epam.sprynchan.web_project.dao.implementation.UserDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Order;
import com.epam.sprynchan.web_project.model.domain.Product;
import com.epam.sprynchan.web_project.model.domain.User;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that changes the locale and load needed list if its necessary
 * @author Kirill
 */

public class ChangeLocale extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_CHECKING_ORDERS = "forward.check.orders";
    private static final String FORWARD_CHECKING_USERS = "forward.checking.users";
    private static final String FORWARD_DELETE_PRODUCT = "forward.delete.product";
    private static final String FORWARD_EDIT_PRODUCTS = "forward.edit.products";
    private static final String FORWARD_PRODUCTS_TABLE = "forward.product.table";
    private static final String FORWARD_ORDERS_USER = "forward.orders_user";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_LANGUAGE = "locale";
    private static final String PARAMETER_PAGE = "forwardPage";
    private static final String PARAMETER_ORDERS_LIST = "orderList";
    private static final String PARAMETER_USERS_LIST = "userList";
    private static final String PARAMETER_PRODUCTS_LIST = "productList";

    /**
     * Method that changes the locale and load needed list if its necessary
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String locale = request.getParameter(PARAMETER_LANGUAGE);
        String forwardPage = request.getParameter(PARAMETER_PAGE);

        setForward(forwardPage);
        request.getSession().setAttribute(PARAMETER_LANGUAGE, locale);

        if (forwardPage.equals(Resource.getStr(FORWARD_CHECKING_ORDERS)) || forwardPage.equals(Resource.getStr(FORWARD_ORDERS_USER))) {
            try {
                OrderDao orderDao = new OrderDao();
                List<Order> orderList = orderDao.buildList();
                request.setAttribute(PARAMETER_ORDERS_LIST, orderList);
            } catch (DAOException e) {
                throw new CommandException(e);
            }
        } else if (forwardPage.equals(Resource.getStr(FORWARD_CHECKING_USERS))) {
            try {
                UserDao userDao = new UserDao();
                List<User> userList = userDao.buildList();
                request.setAttribute(PARAMETER_USERS_LIST, userList);
            } catch (DAOException e) {
                throw new CommandException(e);
            }
        } else if (forwardPage.equals(Resource.getStr(FORWARD_DELETE_PRODUCT))
                || forwardPage.equals(Resource.getStr(FORWARD_EDIT_PRODUCTS)) || forwardPage.equals(Resource.getStr(FORWARD_PRODUCTS_TABLE))) {
            try {
                ProductDao productDao = new ProductDao();
                List<Product> productList = productDao.buildList();
                request.setAttribute(PARAMETER_PRODUCTS_LIST, productList);
            } catch (DAOException e) {
                throw new CommandException(e);
            }
        }
    }
}
