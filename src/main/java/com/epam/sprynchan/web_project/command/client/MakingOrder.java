package com.epam.sprynchan.web_project.command.client;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.OrderDao;
import com.epam.sprynchan.web_project.dao.implementation.ProductDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.builders.OrderBuilder;
import com.epam.sprynchan.web_project.model.domain.Order;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class implements a pattern command
 * This class that makes order of product
 * @author Kirill
 */

public class MakingOrder extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_PRODUCTS_TABLE = "forward.product.table";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_PRODUCTS_LIST = "productList";

    /**
     * Method that makes order of product
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Order order;
        OrderDao orderDao = new OrderDao();
        ProductDao productDao = new ProductDao();

        try {
            OrderBuilder orderBuilder = new OrderBuilder();
            if (orderBuilder.build(request)) {
                order = orderBuilder.getOrder();

                orderDao.createOrder(order);

                request.setAttribute(PARAMETER_PRODUCTS_LIST, productDao.buildList());
                setForward(Resource.getStr(FORWARD_PRODUCTS_TABLE));
            } else {

            }
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
