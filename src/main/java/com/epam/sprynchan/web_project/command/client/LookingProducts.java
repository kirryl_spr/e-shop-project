package com.epam.sprynchan.web_project.command.client;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.ProductDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Product;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that shows to user catalog of products
 * @author Kirill
 */

public class LookingProducts extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_PRODUCTS_TABLE = "forward.product.table";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_PRODUCTS_LIST = "productList";

    /**
     * Method that shows to user catalog of products
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        List<Product> productList;
        ProductDao productDao = new ProductDao();

        try {
            productList = productDao.buildList();

            request.setAttribute(PARAMETER_PRODUCTS_LIST, productList);
            setForward(Resource.getStr(FORWARD_PRODUCTS_TABLE));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
