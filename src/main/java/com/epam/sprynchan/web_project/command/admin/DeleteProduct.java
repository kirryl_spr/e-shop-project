package com.epam.sprynchan.web_project.command.admin;


import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.ProductDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Product;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that deletes products from database
 * @author Kirill
 */

public class DeleteProduct extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_DELETE_PRODUCT = "forward.delete.product";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_ID_PRODUCT = "idProduct";
    private static final String PARAMETER_PRODUCTS_LIST = "productList";

    /**
     * Method that deletes products from database
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        ProductDao productDao = new ProductDao();
        List<Product> productList;

        try {
            int idProduct = Integer.parseInt(request.getParameter(PARAMETER_ID_PRODUCT));

            productDao.deleteProduct(idProduct);

            productList = productDao.buildList();

            request.setAttribute(PARAMETER_PRODUCTS_LIST, productList);
            setForward(Resource.getStr(FORWARD_DELETE_PRODUCT));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
