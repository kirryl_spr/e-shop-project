package com.epam.sprynchan.web_project.command.admin;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.ProductDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Product;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that performs edition of product in database
 * @author Kirill
 */

public class PerformEdition extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_EDIT_CAR = "forward.edit.products";
    private static final String DONE_MESSAGE = "done_edit_product_message";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_NAME = "name";
    private static final String PARAMETER_PRICE = "price";
    private static final String PARAMETER_AMOUNT = "amount";
    private static final String PARAMETER_CATEGORY = "category";
    private static final String PARAMETER_ID_PRODUCT = "idProduct";
    private static final String PARAMETER_PRODUCTS_LIST = "productList";
    private static final String PARAMETER_DONE = "DONE";

    /**
     * Method that performs edition of product in database
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        ProductDao productDao = new ProductDao();
        List<Product> productList;

        try {
            Product product = new Product();

            product.setIdProduct(Integer.parseInt(request.getParameter(PARAMETER_ID_PRODUCT)));
            product.setName(request.getParameter(PARAMETER_NAME));
            product.setPrice(Integer.parseInt(request.getParameter(PARAMETER_PRICE)));
            product.setAmount(Integer.parseInt(request.getParameter(PARAMETER_AMOUNT)));
            product.setCategory(request.getParameter(PARAMETER_CATEGORY));

            productDao.editProduct(product);

            productList = productDao.buildList();

            request.setAttribute(PARAMETER_DONE, DONE_MESSAGE);
            request.setAttribute(PARAMETER_PRODUCTS_LIST, productList);
            setForward(Resource.getStr(FORWARD_EDIT_CAR));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }

    }
}
