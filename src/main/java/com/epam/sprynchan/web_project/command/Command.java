package com.epam.sprynchan.web_project.command;

import com.epam.sprynchan.web_project.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class implements a pattern command
 * This class causes methods a business-logic and sends results on jsp
 *
 * @author Kirill
 */
public abstract class Command {
    /**
     * This address to which will is realized transition after performing the command
     */
    private String forward;

    public String getForward() {
        return forward;
    }

    public void setForward(String forward) {
        this.forward = forward;
    }

    /**
     * This is abstract method which causes methods a business-logic and sends results on jsp
     *
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws com.epam.sprynchan.web_project.command.exception.CommandException a ServletException
     */
    public abstract void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
