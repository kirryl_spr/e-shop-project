package com.epam.sprynchan.web_project.command;

import com.epam.sprynchan.web_project.command.admin.*;
import com.epam.sprynchan.web_project.command.client.LookingProducts;
import com.epam.sprynchan.web_project.command.client.MakingOrder;
import com.epam.sprynchan.web_project.command.client.OrdersLook;
import com.epam.sprynchan.web_project.command.client.Payment;
import com.epam.sprynchan.web_project.command.entrance.Authorization;
import com.epam.sprynchan.web_project.command.entrance.Registration;
import com.epam.sprynchan.web_project.command.entrance.SignOut;
import com.epam.sprynchan.web_project.command.locale.ChangeLocale;
import com.epam.sprynchan.web_project.command.navigate.AddingProduct;
import com.epam.sprynchan.web_project.command.navigate.DeletingProduct;
import com.epam.sprynchan.web_project.command.navigate.EditingProduct;
import com.epam.sprynchan.web_project.command.navigate.ToPage;
import com.epam.sprynchan.web_project.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * This is class which implements a pattern factory
 * This class get a commandName from request, checks this name
 * and return a instance of class <code>Command</code>
 *
 * @author Kirill
 */
public class CommandFactory {
    /**
     * String for command parameter parsing
     */
    private static final String COMMAND = "command";
    /**
     * Strings contains keys for errors in resource bundle
     */
    private static final String ERROR_NO_SUCH_COMMAND = "error.no.such.command";
    private static final String ERROR_IN_GETTING_COMMAND = "error.in.getting.command";
    private static final String ERROR_IN_CREATING_COMMAND = "error.in.creating.command";

    /**
     * This method get a commandName from request, checks this name and return
     * a instance of class <code>Command</code>
     *
     * @param request a HttpServletRequest
     * @return a new <code>Command</code>, this a instance of class which implements
     * a pattern command
     */
    public static Command getCommand(HttpServletRequest request) throws CommandException {
        Command command;
        CommandEnum commandType = getCommandEnum(request.getParameter(COMMAND));

        switch (commandType) {
            case CHANGE_LOCALE:
                command = new ChangeLocale();
                break;
            case REGISTRATION:
                command = new Registration();
                break;
            case TO_PAGE:
                command = new ToPage();
                break;
            case AUTHORIZATION:
                command = new Authorization();
                break;
            case SIGN_OUT:
                command = new SignOut();
                break;
            case LOOKING_PRODUCTS:
                command = new LookingProducts();
                break;
            case MAKING_ORDER:
                command = new MakingOrder();
                break;
            case LOOKING_ORDERS:
                command = new OrdersLook();
                break;
            case PAYMENT:
                command = new Payment();
                break;
            case ADDING_PRODUCT:
                command = new AddingProduct();
                break;
            case ADD_PRODUCT:
                command = new AddProduct();
                break;
            case DELETING_PRODUCT:
                command = new DeletingProduct();
                break;
            case DELETE_PRODUCT:
                command = new DeleteProduct();
                break;
            case CHECKING_ORDERS:
                command = new CheckingOrders();
                break;
            case CHECKING_USERS:
                command = new CheckingUsers();
                break;
            case IN_BLACKLIST:
                command = new BlacklistIn();
                break;
            case OUT_BLACKLIST:
                command = new BlacklistOut();
                break;
            case EDITING_PRODUCT:
                command = new EditingProduct();
                break;
            case EDIT_PRODUCT:
                command = new EditProduct();
                break;
            case PERFORM_EDITION:
                command = new PerformEdition();
                break;
            default:
                throw new CommandException(ERROR_NO_SUCH_COMMAND);
        }
        return command;
    }

    /**
     * This method creats a instance of <code>CommandEnum</code> by a command name
     * If command name is incorrect then <code>CommandEnum.UNKNOWN_COMMAND</code> will return
     *
     * @param command a name of command
     * @return a instance of <code>CommandEnum</code>
     */
    private static CommandEnum getCommandEnum(String command) throws CommandException {
        CommandEnum commandEnum;

        try {
            commandEnum = CommandEnum.valueOf(command);
        } catch (IllegalArgumentException ex) {
            throw new CommandException(ERROR_IN_GETTING_COMMAND, ex);
        } catch (NullPointerException ex) {
            throw new CommandException(ERROR_IN_CREATING_COMMAND, ex);
        }
        return commandEnum;
    }
}
