package com.epam.sprynchan.web_project.command.navigate;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.dao.implementation.ProductDao;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.model.domain.Product;
import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * This class implements a pattern command
 * This class that forward user to editing product page and loads the list of categories
 * @author Kirill
 */

public class EditingProduct extends Command {

    /**
     * Keys to bundle
     */
    private static final String FORWARD_EDIT_PRODUCTS = "forward.edit.products";

    /**
     * Parameters for working with request
     */
    private static final String PARAMETER_PRODUCTS_LIST = "productList";

    /**
     * Method that forward user to editing product page and loads the list of categories
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws CommandException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        ProductDao productDao = new ProductDao();
        List<Product> productList;

        try {
            productList = productDao.buildList();

            request.setAttribute(PARAMETER_PRODUCTS_LIST, productList);
            setForward(Resource.getStr(FORWARD_EDIT_PRODUCTS));
        } catch (DAOException e) {
            throw new CommandException(e.getMessage());
        }
    }
}
