package com.epam.sprynchan.web_project.tags;

import com.epam.sprynchan.web_project.resource.Resource;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * This tag outputs the footer
 * It also outputs menu to navigations between page
 *
 * @author Kirill
 */
public class FooterLayout extends TagSupport {
    /**
     * Strings that contain keys for code in bundle
     */
    private static final String FIXED_FOOTER = "fixed.footer";
    private static final String FOOTER = "footer";

    /**
     * Parameter of choosing footer
     */
    private String isFixed;

    public String getIsFixed() {
        return isFixed;
    }

    public void setIsFixed(String isFixed) {
        this.isFixed = isFixed;
    }

    /**
     * Method that choosing which footer is needed to be print, and print it
     *
     * @return SKIP_BODY
     * @throws JspException
     */
    public int doStartTag() throws JspException {
        try {
            if (isFixed.equals("true")) {
                JspWriter writer = pageContext.getOut();
                writer.write(Resource.getStr(FIXED_FOOTER));
            } else {
                JspWriter writer = pageContext.getOut();
                writer.write(Resource.getStr(FOOTER));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
