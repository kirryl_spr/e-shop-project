package com.epam.sprynchan.web_project.model.domain;

/**
 * Class describing category entity from data-base
 *
 * @author Kirill
 */
public class Category {
    /**
     * ID of category
     */
    private int idCategory;
    /**
     * Name of category
     */
    private String category;

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category1 = (Category) o;

        if (idCategory != category1.idCategory) return false;
        if (!category.equals(category1.category)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCategory;
        result = 33 * result + category.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return getClass().getName() +
                "idCategory=" + idCategory +
                ", category='" + category + '\'' +
                '}';
    }
}
