package com.epam.sprynchan.web_project.model.domain;

import java.sql.Date;

/**
 * Class describing order entity from data-base
 *
 * @author Kirill
 */
public class Order {
    /**
     * Order ID
     */
    private int idOrders;
    /**
     * ID of user that made order
     */
    private int idUser;
    /**
     * Amount of product in order
     */
    private int amount;
    /**
     * ID of product in order
     */
    private int idProduct;
    /**
     * Name of product in order
     */
    private String product;
    /**
     * Login of user
     */
    private String login;
    /**
     * Price of order
     */
    private int price;
    /**
     * Date of order
     */
    private Date dateOfOrder;
    /**
     * Variable that shows whether the order is paid
     */
    private boolean isPaid;

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getIdOrders() {
        return idOrders;
    }

    public void setIdOrders(int idOrders) {
        this.idOrders = idOrders;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(Date dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        this.isPaid = paid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (amount != order.amount) return false;
        if (idOrders != order.idOrders) return false;
        if (idProduct != order.idProduct) return false;
        if (idUser != order.idUser) return false;
        if (isPaid != order.isPaid) return false;
        if (price != order.price) return false;
        if (!dateOfOrder.equals(order.dateOfOrder)) return false;
        if (!login.equals(order.login)) return false;
        if (!product.equals(order.product)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idOrders;
        result = 33 * result + idUser;
        result = 33 * result + amount;
        result = 33 * result + idProduct;
        result = 33 * result + product.hashCode();
        result = 33 * result + login.hashCode();
        result = 33 * result + price;
        result = 33 * result + dateOfOrder.hashCode();
        result = 33 * result + (isPaid ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return getClass().getName() +
                "idOrders=" + idOrders +
                ", idUser=" + idUser +
                ", amount=" + amount +
                ", idProduct=" + idProduct +
                ", product='" + product + '\'' +
                ", login='" + login + '\'' +
                ", price=" + price +
                ", dateOfOrder=" + dateOfOrder +
                ", isPaid=" + isPaid +
                '}';
    }
}
