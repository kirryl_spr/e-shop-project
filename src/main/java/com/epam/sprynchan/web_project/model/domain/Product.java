package com.epam.sprynchan.web_project.model.domain;

/**
 * Class describing product entity from data base
 *
 * @author Kirill
 */
public class Product {
    /**
     * Product ID
     */
    private int idProduct;
    /**
     * Name of the product
     */
    private String name;
    /**
     * Price per one item of product
     */
    private int price;
    /**
     * Amount of product that are in stock
     */
    private int amount;
    /**
     * Category of product
     */
    private String category;

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (amount != product.amount) return false;
        if (idProduct != product.idProduct) return false;
        if (price != product.price) return false;
        if (!category.equals(product.category)) return false;
        if (!name.equals(product.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idProduct;
        result = 32 * result + name.hashCode();
        result = 32 * result + price;
        result = 32 * result + amount;
        result = 32 * result + category.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return getClass().getName() +
                "idProduct=" + idProduct +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                ", category='" + category + '\'' +
                '}';
    }
}
