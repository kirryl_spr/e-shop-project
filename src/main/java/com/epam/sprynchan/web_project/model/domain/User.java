package com.epam.sprynchan.web_project.model.domain;

/**
 * Class describing User entity from database
 *
 * @author Kirill
 */
public class User {
    /**
     * ID of user
     */
    private int userID;
    /**
     * Login of user in system
     */
    private String login;
    /**
     * Password of user
     */
    private String password;
    /**
     * Email of user
     */
    private String email;
    /**
     * Variable that show whether user is in blacklist
     */
    private boolean blacklist;
    /**
     * Variable that shows whethet the user is admin
     */
    private boolean isAdmin;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isBlacklist() {
        return blacklist;
    }

    public void setBlacklist(boolean blacklist) {
        this.blacklist = blacklist;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (blacklist != user.blacklist) return false;
        if (isAdmin != user.isAdmin) return false;
        if (userID != user.userID) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userID;
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (blacklist ? 1 : 0);
        result = 31 * result + (isAdmin ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return getClass().getName() +
                "userID=" + userID +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", blacklist=" + blacklist +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
