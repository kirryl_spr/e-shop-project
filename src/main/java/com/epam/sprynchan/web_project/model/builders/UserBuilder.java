package com.epam.sprynchan.web_project.model.builders;

import com.epam.sprynchan.web_project.model.domain.User;

import javax.servlet.http.HttpServletRequest;

/**
 * This class gets user data from
 * request and builds a user
 *
 * @author Kirill
 */
public class UserBuilder {
    /**
     * Parametetres for building user from request
     */
    private static final String PARAMETER_LOGIN = "login";
    private static final String PARAMETER_PASSWORD = "password";
    private static final String PARAMETER_EMAIL = "email";
    /**
     * This is new user which  will is built
     */
    private User user = new User();

    /**
     * This method builds a user
     * This method read all user properties, if each proterty
     * is successfully read, that user is successfully built
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if user has built successfully
     */
    public boolean build(HttpServletRequest request) {
        boolean readingSuccessfull;
        readingSuccessfull = readLogin(request);
        readingSuccessfull &= readPassword(request);
        readingSuccessfull &= readEmail(request);
        return readingSuccessfull;
    }

    /**
     * This method gets login from request and check it
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if login is correct
     */
    private boolean readLogin(HttpServletRequest request) {
        String login = request.getParameter(PARAMETER_LOGIN);
        if (login != null) {
            user.setLogin(login);
            return true;
        }
        return false;
    }

    /**
     * This method gets password from request and check it
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if password is correct and is equal to confirm password
     */
    private boolean readPassword(HttpServletRequest request) {
        String password = request.getParameter(PARAMETER_PASSWORD);
        if (password != null) {
            user.setPassword(password);
            return true;
        }
        return false;
    }

    /**
     * This method gets e-mail from request and check it
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if e-mail is correct
     */
    private boolean readEmail(HttpServletRequest request) {
        String email = request.getParameter(PARAMETER_EMAIL);
        if (email != null) {
            user.setEmail(email);
            return true;
        }
        return false;
    }

    public User getUser() {
        return user;
    }
}
