package com.epam.sprynchan.web_project.model.builders;

import com.epam.sprynchan.web_project.model.domain.Order;
import com.epam.sprynchan.web_project.model.domain.User;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * This class gets order data from
 * request and builds a order
 *
 * @author Kirill
 */
public class OrderBuilder {
    /**
     * Parametetres for building order from request
     */
    private static final String PARAMETER_USER = "user";
    private static final String PARAMETER_PRICE = "price";
    private static final String PARAMETER_AMOUNT = "amount";
    private static final String PARAMETER_ID_PRODUCT = "idProduct";
    /**
     * This is new order which  will is built
     */
    private Order order = new Order();

    /**
     * This method builds a order
     * This method read all user properties, if each property
     * is successfully read, that order is successfully built
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if order has built successfully
     */
    public boolean build(HttpServletRequest request) {
        boolean buildingSuccessful;
        buildingSuccessful = readDate();
        buildingSuccessful &= readAmount(request);
        buildingSuccessful &= readPrice(request);
        buildingSuccessful &= readPaid();
        buildingSuccessful &= readUserID(request);
        buildingSuccessful &= readIdProduct(request);
        return buildingSuccessful;
    }

    /**
     * This method gets data from pc setttings and check it
     *
     * @return <code>true</code> if login is correct
     */
    private boolean readDate() {
        long curTime = System.currentTimeMillis();
        Date date = new Date(curTime);
        order.setDateOfOrder(date);
        return true;
    }

    /**
     * This method gets price from request and check it
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if price is correct
     */
    private boolean readPrice(HttpServletRequest request) {
        int price = -1;
        price = Integer.parseInt(request.getParameter(PARAMETER_PRICE));
        if (price != -1) {
            order.setPrice(price * order.getAmount());
            return true;
        }
        return false;
    }

    /**
     * This method make order unpaid by default
     *
     * @return <code>true</code> if price is correct
     */
    private boolean readPaid() {
        order.setPaid(false);
        return true;
    }

    /**
     * This method gets user from request and check it
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if user is correct
     */
    private boolean readUserID(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute(PARAMETER_USER);
        if (user != null) {
            order.setIdUser(user.getUserID());
            return true;
        }
        return false;
    }

    /**
     * This method gets idProduct from request and check it
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if idProduct is correct
     */
    private boolean readIdProduct(HttpServletRequest request) {
        int productID = -1;
        productID = Integer.parseInt(request.getParameter(PARAMETER_ID_PRODUCT));
        if (productID != -1) {
            order.setIdProduct(productID);
            return true;
        }
        return false;
    }

    /**
     * This method gets amount from request and check it
     *
     * @param request a HttpServletRequest
     * @return <code>true</code> if amount is correct
     */
    private boolean readAmount(HttpServletRequest request) {
        int amount = -1;
        amount = Integer.parseInt(request.getParameter(PARAMETER_AMOUNT));
        if (amount != -1) {
            order.setAmount(amount);
            return true;
        }
        return false;
    }

    public Order getOrder() {
        return order;
    }
}
