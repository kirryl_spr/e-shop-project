package com.epam.sprynchan.web_project.util;

/**
 * This is enum of user type
 *
 * @author Kirill
 */
public enum UserTypeEnum {
    ALL, ADMIN, USER
}
