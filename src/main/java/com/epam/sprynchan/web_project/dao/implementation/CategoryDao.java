package com.epam.sprynchan.web_project.dao.implementation;

import com.epam.sprynchan.web_project.dao.Dao;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.dao.pool.exception.ConnectionPoolException;
import com.epam.sprynchan.web_project.model.domain.Category;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for working with the category table from database
 * @author Kirill
 */

public class CategoryDao implements Dao {

    /**
     * SQL-statements
     */
    private static final String GET_CATEGORIES = "SELECT idCategory, categoryName FROM category";

    /**
     * Keys to bundle for error causes
     */
    private static final String ERROR_IN_BUILDING_LIST = "error.in.building.list.of.categories";


    /**
     * Overrided method from Dao interface. Builds list of categories.
     *
     * @return List
     * @throws DAOException
     */
    @Override
    public List<Category> buildList() throws DAOException {
        Connection connector = null;
        ResultSet resultSet;
        List<Category> categoryList = new ArrayList<Category>();

        try {
            connector = dataBaseConnection.getConnection();

            Statement statement = connector.createStatement();

            resultSet = statement.executeQuery(GET_CATEGORIES);

            while (resultSet.next()) {
                Category category = new Category();
                category.setIdCategory(resultSet.getInt(1));
                category.setCategory(resultSet.getString(2));
                categoryList.add(category);
            }

        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_BUILDING_LIST, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
        return categoryList;
    }
}
