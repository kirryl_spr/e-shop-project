package com.epam.sprynchan.web_project.dao.implementation;

import com.epam.sprynchan.web_project.dao.Dao;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.dao.pool.exception.ConnectionPoolException;
import com.epam.sprynchan.web_project.model.domain.Order;
import com.epam.sprynchan.web_project.model.domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for working with the order table from database
 * @author Kirill
 */

public class OrderDao implements Dao {

    /**
     * SQL-statements
     */
    private static final String CREATE_ORDER = "INSERT INTO orders(User_idUser, Product_idProduct, " +
                                               "dateOfOrder, totalPrice, isPaid, totalAmount) VALUES(?,?,?,?,?,?)";

    private static final String GET_ORDERS_BY_USER = "SELECT idOrders, product.name, dateOfOrder, " +
                                                     "totalPrice, isPaid FROM orders INNER JOIN product" +
                                                     " ON orders.Product_idProduct=product.idProduct " +
                                                     "WHERE User_idUser=(?)";

    private static final String PAY_FOR_ORDER = "UPDATE orders SET isPaid=TRUE WHERE idOrders=(?)";

    private static final String CHANGE_AMOUNT = "UPDATE product SET amount=(amount - (?)) WHERE idProduct=(?)";

    private static final String GET_ORDERS = "SELECT idOrders, user.login, product.name, totalAmount, totalPrice, " +
                                             "dateOfOrder, isPaid FROM orders " +
                                             "INNER JOIN user ON orders.User_idUser=user.idUser " +
                                             "INNER JOIN product ON orders.Product_idProduct=product.idProduct";

    /**
     * Keys to bundle for error causes
     */
    private static final String ERROR_IN_PAYMENT = "error.in.payment";

    private static final String ERROR_IN_BUILDING_LIST_ADMIN = "error.in.building.list.of.orders.admin";

    private static final String ERROR_IN_BUILDING_LIST = "error.in.building.list.of.orders";

    private static final String ERROR_IN_CREATING_ORDER = "error.in.creating.order";


    /**
     * Method that inserting new order in database
     *
     * @param order
     * @throws DAOException
     */
    public void createOrder(Order order) throws DAOException {
        Connection connector = null;

        try {
            connector = dataBaseConnection.getConnection();

            PreparedStatement preparedStatement = connector.prepareStatement(CREATE_ORDER);
            preparedStatement.setInt(1, order.getIdUser());
            preparedStatement.setInt(2, order.getIdProduct());
            preparedStatement.setDate(3, order.getDateOfOrder());
            preparedStatement.setInt(4, order.getPrice());
            preparedStatement.setBoolean(5, order.isPaid());
            preparedStatement.setInt(6, order.getAmount());
            preparedStatement.executeUpdate();

            preparedStatement = connector.prepareStatement(CHANGE_AMOUNT);
            preparedStatement.setInt(1, order.getAmount());
            preparedStatement.setInt(2, order.getIdProduct());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_CREATING_ORDER, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
    }

    /**
     * Method that list of orders for user using his id
     * @param user
     * @return
     * @throws DAOException
     */
    public List<Order> buildListOfOrders(User user) throws DAOException {
        Connection connector = null;
        ResultSet resultSet;
        List<Order> listOfOrders = new ArrayList<Order>();

        try {
            connector = dataBaseConnection.getConnection();

            PreparedStatement preparedStatement = connector.prepareStatement(GET_ORDERS_BY_USER);
            preparedStatement.setInt(1, user.getUserID());

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                Order order = new Order();
                order.setIdOrders(resultSet.getInt(1));
                order.setProduct(resultSet.getString(2));
                order.setDateOfOrder(resultSet.getDate(3));
                order.setPrice(resultSet.getInt(4));
                order.setPaid(resultSet.getBoolean(5));

                listOfOrders.add(order);
            }

        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_BUILDING_LIST, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
        return listOfOrders;
    }

    /**
     * Overrided method from Dao interface. Builds list of orders.
     *
     * @return List
     * @throws DAOException
     */
    @Override
    public List<Order> buildList() throws DAOException {
        Connection connector = null;
        ResultSet resultSet;
        List<Order> listOfOrders = new ArrayList<Order>();

        try {
            connector = dataBaseConnection.getConnection();

            Statement statement = connector.createStatement();
            resultSet = statement.executeQuery(GET_ORDERS);

            while (resultSet.next()) {
                Order order = new Order();
                order.setIdOrders(resultSet.getInt(1));
                order.setLogin(resultSet.getString(2));
                order.setProduct(resultSet.getString(3));
                order.setAmount(resultSet.getInt(4));
                order.setPrice(resultSet.getInt(5));
                order.setDateOfOrder(resultSet.getDate(6));
                order.setPaid(resultSet.getBoolean(7));
                listOfOrders.add(order);
            }
        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_BUILDING_LIST_ADMIN, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
        return listOfOrders;
    }

    /**
     * Method that makes order paid
     * @param idOrder
     * @throws DAOException
     */
    public void payForOrder(int idOrder) throws DAOException {
        Connection connector = null;

        try {
            connector = dataBaseConnection.getConnection();

            PreparedStatement preparedStatement = connector.prepareStatement(PAY_FOR_ORDER);
            preparedStatement.setInt(1, idOrder);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_PAYMENT, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
    }
}
