package com.epam.sprynchan.web_project.dao.pool;

import com.epam.sprynchan.web_project.dao.pool.exception.ConnectionPoolException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @author Kirill
 *         <p/>
 *         The class <code>ConnectionPool</code>implements+
 *         the connection pool to the database sql.
 */
public class ConnectionPool {

    /**
     * Singleton instance
     */
    private static volatile ConnectionPool instance;

    /**
     * Configuration constants for the need to create a pool
     */
    private static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/e-shop";
    private static final String LOGIN = "root";
    private static final String PASSWORD = "kirill94";
    private static final int MAX_CONNECTION_COUNT = 10;
    private static final int MIN_CONNECTION_COUNT = 5;

    /**
     * Keys to bundle for error messages
     */
    private static final String ERROR_DRIVER = "error.driver";
    private static final String ERROR_INTERRUPTED = "error.interrupted";

    /**
     * Variable leading accounting of current connections
     */
    private volatile int currentConnectionNumber = MIN_CONNECTION_COUNT;
    private BlockingQueue<Connection> pool = new ArrayBlockingQueue<Connection>(MAX_CONNECTION_COUNT, true);

    /**
     * Singleton of connection pool
     *
     * @return instance
     */
    public static ConnectionPool getInstance() {
        if (instance == null) {
            synchronized (ConnectionPool.class) {
                if (instance == null) {
                    instance = new ConnectionPool();
                }
            }
        }
        return instance;
    }

    /**
     * The constructor creates an instance of the pool.
     * Initializes a constant number of connections - 5.
     */
    public ConnectionPool() {
        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException e) {

        }
        for (int i = 0; i < MIN_CONNECTION_COUNT; i++) {
            try {
                pool.add(DriverManager.getConnection(URL, LOGIN, PASSWORD));
            } catch (SQLException e) {

            }
        }
    }

    /**
     * The method provides the user with a copy of connection from pool
     *
     * @return Connection
     * @see java.sql.Connection
     */
    public Connection getConnection() throws ConnectionPoolException {
        Connection connection;
        try {
            if (pool.isEmpty() && currentConnectionNumber < MAX_CONNECTION_COUNT) {
                openAdditionalConnection();
            }
            connection = pool.take();
        } catch (InterruptedException ex) {
            throw new ConnectionPoolException(ERROR_INTERRUPTED, ex);
        }
        return connection;
    }

    /**
     * The method is returned, the connection back to the pool
     * when you are finished work with him.
     *
     * @param connection
     */
    public void closeConnection(Connection connection) throws ConnectionPoolException {
        if (connection != null) {

            if (currentConnectionNumber > MIN_CONNECTION_COUNT) {
                currentConnectionNumber--;
            }

            try {
                pool.put(connection);
            } catch (InterruptedException ex) {
                throw new ConnectionPoolException(ERROR_INTERRUPTED, ex);
            }

        }
    }

    /**
     * Method , if necessary, to provide additional
     * connection to the database
     */
    private void openAdditionalConnection() throws ConnectionPoolException {
        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException exception) {
            throw new ConnectionPoolException(exception);
        }
        try {
            pool.add(DriverManager.getConnection(URL, LOGIN, PASSWORD));
            currentConnectionNumber++;
        } catch (SQLException ex) {
            throw new ConnectionPoolException(ERROR_DRIVER, ex);
        }
    }

}
