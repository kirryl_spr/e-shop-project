package com.epam.sprynchan.web_project.dao.pool.exception;

import com.epam.sprynchan.web_project.dao.exception.DAOException;

/**
 * @author Kiiril
 * @see com.epam.sprynchan.web_project.dao.exception.DAOException
 * @since 1.8
 * <p/>
 * <code>ConnectionPoolException</code>
 * <p/>
 * Exception class created specifically to describe the exceptional
 * situation arises in the Connection pool layer application.
 */
public class ConnectionPoolException extends DAOException {
    public ConnectionPoolException() {
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionPoolException(Throwable cause) {
        super(cause);
    }

    public ConnectionPoolException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
