package com.epam.sprynchan.web_project.dao.implementation;

import com.epam.sprynchan.web_project.dao.Dao;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.dao.pool.exception.ConnectionPoolException;
import com.epam.sprynchan.web_project.model.domain.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for working with the product table from database
 * @author Kirill
 */
public class ProductDao implements Dao {

    /**
     * SQL-statements
     */
    private static final String GET_PRODUCT_SCRIPT = "SELECT idProduct, name,  price, amount, category.categoryName " +
                                                     "FROM product INNER JOIN category ON product.Category_idCategory=category.idCategory";

    private static final String GET_CATEGORY = "SELECT idCategory FROM category WHERE categoryName=(?)";

    private static final String DELETE_PRODUCT = "DELETE FROM product WHERE idProduct=?";

    private static final String UPDATE_PRODUCT = "UPDATE product SET name=(?), price=(?), amount=(?)," +
                                                 " Category_idCategory=(?) WHERE idProduct=(?)";

    private static final String ADD_PRODUCT = "INSERT INTO product(name, price, amount, Category_idCategory)" +
                                              " VALUES(?,?,?,?)";

    /**
     * Keys to bundle for error causes
     */
    private static final String ERROR_IN_EDITING_PRODUCT = "error.in.editing.product";

    private static final String ERROR_IN_BUILDING_LIST = "error.in.building.list.of.products";

    private static final String ERROR_IN_DELETING_PRODUCT_FROM_DB = "error.in.deleting.product.from.db";

    private static final String ERROR_NAME_CONFLICT = "error.name.conflict";

    /**
     * Overrided method from Dao interface. Builds list of products.
     *
     * @return List
     * @throws DAOException
     */
    @Override
    public List buildList() throws DAOException {
        Connection connector = null;
        ResultSet resultSet;
        List<Product> productList = new ArrayList<Product>();

        try {
            connector = dataBaseConnection.getConnection();

            Statement statement = connector.createStatement();
            resultSet = statement.executeQuery(GET_PRODUCT_SCRIPT);

            while (resultSet.next()) {
                Product product = new Product();
                product.setIdProduct(resultSet.getInt(1));
                product.setName(resultSet.getString(2));
                product.setPrice(resultSet.getInt(3));
                product.setAmount(resultSet.getInt(4));
                product.setCategory(resultSet.getString(5));
                productList.add(product);
            }
        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_BUILDING_LIST, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
        return productList;
    }

    /**
     * Method that inserting new product in database
     *
     * @param name
     * @param category
     * @param price
     * @param amount
     * @throws DAOException
     */
    public void addProduct(String name, String category, int price, int amount) throws DAOException {
        Connection connector = null;
        ResultSet resultSet;

        try {
            connector = dataBaseConnection.getConnection();

            int idCategory;

            PreparedStatement preparedStatement = connector.prepareStatement(GET_CATEGORY);
            preparedStatement.setString(1, category);

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                idCategory = resultSet.getInt(1);
                preparedStatement = connector.prepareStatement(ADD_PRODUCT);
                preparedStatement.setString(1, name);
                preparedStatement.setInt(2, price);
                preparedStatement.setInt(3, amount);
                preparedStatement.setInt(4, idCategory);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException(ERROR_NAME_CONFLICT, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
    }

    /**
     * Method that deletes product from database
     *
     * @param idProduct
     * @throws DAOException
     */
    public void deleteProduct(int idProduct) throws DAOException {
        Connection connector = null;

        try {
            connector = dataBaseConnection.getConnection();

            PreparedStatement preparedStatement = connector.prepareStatement(DELETE_PRODUCT);
            preparedStatement.setInt(1, idProduct);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_DELETING_PRODUCT_FROM_DB, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
    }

    /**
     * Method that edit product
     *
     * @param product
     * @throws DAOException
     */
    public void editProduct(Product product) throws DAOException {
        Connection connector = null;
        ResultSet resultSet;

        try {
            connector = dataBaseConnection.getConnection();

            int idCategory;

            PreparedStatement preparedStatement = connector.prepareStatement(GET_CATEGORY);
            preparedStatement.setString(1, product.getCategory());

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                idCategory = resultSet.getInt(1);
                preparedStatement = connector.prepareStatement(UPDATE_PRODUCT);
                preparedStatement.setString(1, product.getName());
                preparedStatement.setInt(2, product.getPrice());
                preparedStatement.setInt(3, product.getAmount());
                preparedStatement.setInt(4, idCategory);
                preparedStatement.setInt(5, product.getIdProduct());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_EDITING_PRODUCT, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
    }
}
