package com.epam.sprynchan.web_project.dao;

import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.dao.pool.ConnectionPool;

import java.util.List;

/**
 * This interface holds objects needed to work with database
 * Also contains a method of building list of entities of domain
 *
 * @author Kirill
 */

public interface Dao {

    ConnectionPool dataBaseConnection = ConnectionPool.getInstance();

    /**
     * Method that builds enities from domain
     *
     * @return
     * @throws DAOException
     * @see com.epam.sprynchan.web_project.model.domain
     */
    public List buildList() throws DAOException;

}
