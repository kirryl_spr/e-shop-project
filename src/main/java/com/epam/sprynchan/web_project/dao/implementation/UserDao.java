package com.epam.sprynchan.web_project.dao.implementation;

import com.epam.sprynchan.web_project.dao.Dao;
import com.epam.sprynchan.web_project.dao.exception.DAOException;
import com.epam.sprynchan.web_project.dao.pool.exception.ConnectionPoolException;
import com.epam.sprynchan.web_project.model.domain.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for working with user table from database
 *
 * @author Kirill
 */

public class UserDao implements Dao {

    /**
     * SQL-statements
     */
    private final static String INSERT_SCRIPT = "INSERT INTO user(login, password, email, blacklist) VALUES(?,?,?,?)";

    private final static String GET_USER_SCRIPT = "SELECT * from user where login=? and  password=?";

    private final static String GET_ALL_USERS_SCRIPT = "SELECT idUser, login, blacklist from user where isAdmin=false";

    private final static String IN_BLACKLIST_SCRIPT = "UPDATE user SET blacklist=TRUE WHERE idUser=(?)";

    private final static String OUT_BLACKLIST_SCRIPT = "UPDATE user SET blacklist=FALSE WHERE idUser=(?)";

    /**
     * Keys to bundle for error causes
     */
    private static final String ERROR_IN_BUILDING_USER = "error.in.building.user";

    private static final String ERROR_IN_BUILDING_LIST = "error.in.building.list.of.users";

    private static final String ERROR_IN_GETTING_USER_FROM_DB = "error.in.getting.user.from.db";

    private static final String ERROR_OUT_BLACKLIST = "error.out.blacklist";

    private static final String ERROR_IN_BLACKLIST = "error.in.blacklist";

    private static final String ERROR_LOGIN_CONFLICT = "error.login.conflict";

    /**
     * Method that inserting new user in database
     *
     * @param user
     * @throws DAOException
     */
    public void insertUser(User user) throws DAOException {
        Connection connector = null;

        try {
            connector = dataBaseConnection.getConnection();

            PreparedStatement preparedStatement = connector.prepareStatement(INSERT_SCRIPT);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setBoolean(4, user.isBlacklist());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(ERROR_LOGIN_CONFLICT, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
    }

    /**
     * Method that puts user in blacklist.
     *
     * @param idUser
     * @throws DAOException
     */
    public void putInBlacklist(int idUser) throws DAOException {
        Connection connector = null;

        try {
            connector = dataBaseConnection.getConnection();

            PreparedStatement preparedStatement = connector.prepareStatement(IN_BLACKLIST_SCRIPT);
            preparedStatement.setInt(1, idUser);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_BLACKLIST, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
    }

    /**
     * Method that puts out user from blacklist.
     *
     * @param idUser
     * @throws DAOException
     */
    public void putOutBlacklist(int idUser) throws DAOException {
        Connection connector = null;

        try {
            connector = dataBaseConnection.getConnection();

            PreparedStatement preparedStatement = connector.prepareStatement(OUT_BLACKLIST_SCRIPT);
            preparedStatement.setInt(1, idUser);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(ERROR_OUT_BLACKLIST, e);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
    }

    /**
     * Method that getting user from data-base by Login ans password
     *
     * @param login
     * @param password
     * @return User
     * @throws DAOException
     */
    public User getUserByLoginPassword(String login, String password) throws DAOException {
        Connection connector = null;
        ResultSet resultSet;
        User user = null;

        try {
            connector = dataBaseConnection.getConnection();

            PreparedStatement statement = connector.prepareStatement(GET_USER_SCRIPT);
            statement.setString(1, login);
            statement.setString(2, password);

            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = buildUser(resultSet);
            }

        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_GETTING_USER_FROM_DB);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }
        return user;
    }

    /**
     * Overrided method from Dao interface. Builds list of users.
     *
     * @return List
     * @throws DAOException
     */
    @Override
    public List buildList() throws DAOException {
        Connection connector = null;
        List<User> list = new ArrayList<User>();
        ResultSet resultSet;

        try {
            connector = dataBaseConnection.getConnection();

            Statement statement = connector.createStatement();
            resultSet = statement.executeQuery(GET_ALL_USERS_SCRIPT);

            while (resultSet.next()) {
                User user = new User();
                user.setUserID(resultSet.getInt(1));
                user.setLogin(resultSet.getString(2));
                user.setBlacklist(resultSet.getBoolean(3));
                list.add(user);
            }

        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_BUILDING_LIST);
        } catch (ConnectionPoolException e) {
            throw new DAOException(e.getMessage());
        } finally {
            dataBaseConnection.closeConnection(connector);
        }

        return list;
    }

    /**
     * Method that building user from ResultSet
     *
     * @param resultSet
     * @return User
     * @throws DAOException
     */
    private User buildUser(ResultSet resultSet) throws DAOException {
        User user = new User();

        try {

            user.setUserID(resultSet.getInt(1));
            user.setLogin(resultSet.getString(2));
            user.setPassword(resultSet.getString(3));
            user.setEmail(resultSet.getString(4));
            user.setBlacklist(resultSet.getBoolean(5));
            user.setAdmin(resultSet.getBoolean(6));

        } catch (SQLException e) {
            throw new DAOException(ERROR_IN_BUILDING_USER, e);
        }

        return user;
    }
}
