package com.epam.sprynchan.web_project.controller;

import com.epam.sprynchan.web_project.command.Command;
import com.epam.sprynchan.web_project.command.CommandFactory;
import com.epam.sprynchan.web_project.command.exception.CommandException;
import com.epam.sprynchan.web_project.resource.Resource;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements the pattern MVC
 * This is Servlet which handles requests
 *
 * @author Kirill
 */

@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class Servlet extends HttpServlet {
    /**
     * This is logger which print some messages to log file
     */
    private static final Logger logger = Logger.getLogger(Servlet.class);
    /**
     * String for redirect ot error page
     */
    private static final String FORWARD_ERROR = "forward.error";
    /**
     * String for message - parameter
     */
    public static final String PARAMETER_MESSAGE = "message";

    /**
     * This method handles requests by get-method
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * This method handles requests by post-method
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * This method gets a instance of <code>Command</code> from <code>CommandFactory</code>
     * by request and execute this command. Then it gos to next jsp.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Command command = CommandFactory.getCommand(request);

            command.processRequest(request, response);

            request.getRequestDispatcher(command.getForward()).forward(request, response);
        } catch (IOException ex) {
            request.setAttribute(PARAMETER_MESSAGE, ex.getMessage());
            request.getRequestDispatcher(Resource.getStr(FORWARD_ERROR)).forward(request, response);
            logger.error(Resource.getStr(ex.getMessage()));
        } catch (CommandException e) {
            request.setAttribute(PARAMETER_MESSAGE, e.getMessage());
            request.getRequestDispatcher(Resource.getStr(FORWARD_ERROR)).forward(request, response);
            logger.error(Resource.getStr(e.getMessage()));
        }
    }
}