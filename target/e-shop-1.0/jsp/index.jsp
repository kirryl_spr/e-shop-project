<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="locale" value="en" scope="session"/>
<html>
<head>
    <title></title>
</head>
<body>
<c:choose>
    <c:when test="${not empty sessionScope.user and sessionScope.user.admin}">
        <jsp:forward page="//controller">
            <jsp:param name="command" value="TO_PAGE"/>
            <jsp:param name="forwardPage" value="jsp/admin/admin_menu.jsp"/>
        </jsp:forward>
    </c:when>
    <c:when test="${not empty sessionScope.user and not sessionScope.user.admin}">
        <jsp:forward page="//controller">
            <jsp:param name="command" value="TO_PAGE"/>
            <jsp:param name="forwardPage" value="jsp/client/client_menu.jsp"/>
        </jsp:forward>
    </c:when>
    <c:otherwise>
        <jsp:forward page="//controller">
            <jsp:param name="command" value="TO_PAGE"/>
            <jsp:param name="forwardPage" value="jsp/entrance/authorization.jsp"/>
        </jsp:forward>
    </c:otherwise>
</c:choose>
</body>
</html>
