<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>

<html>
<head>
  <title>Orders</title>
  <%@include file="../script.jsp" %>
</head>

<body>

<%@include file="../header.jsp"%>

<div class="single">
  <div class="container">
    <div class="bottom">
      <h1 align="center"><fmt:message key="my_orders"/></h1>
      <div id="main">
        <c:choose>
          <c:when test="${not empty orderList }">
            <table id="tablesorter" class="tablesorter" border="0" cellpadding="0" cellspacing="1">
              <thead>
              <tr bgcolor="#F5F5F5">
                <th rowspan="2"><fmt:message key="idOrders"/></th>
                <th rowspan="2"><fmt:message key="name"/></th>
                <th rowspan="2"><fmt:message key="price"/></th>
                <th rowspan="2"><fmt:message key="date"/></th>
                <th rowspan="2"><fmt:message key="payment"/></th>
              </tr>
              </thead>
              <tbody>
              <c:forEach var="order" items="${ orderList }">
                <tr>
                  <form action="/controller" method="post">
                    <td>
                      <input type="hidden" name="idOrders" value="${order.idOrders}">${order.idOrders}
                    </td>
                    <td>
                        ${order.product}
                    </td>
                    <td>
                        ${order.price}
                    </td>
                    <td>
                        ${order.dateOfOrder}
                    </td>
                    <c:choose>
                      <c:when test="${order.paid == false}">
                        <td>
                          <input type="hidden" name="command" value="PAYMENT">
                          <input type="submit" class="sbmt-btn" value="<fmt:message key="pay_for_order"/>">
                        </td>
                      </c:when>
                      <c:otherwise>
                        <td>
                          <fmt:message key="paid.in_progress"/>
                        </td>
                      </c:otherwise>
                    </c:choose>
                  </form>
                </tr>
              </c:forEach>
              </tbody>
            </table>
          </c:when>
          <c:otherwise>
            <h1 align="center"><fmt:message key="no_orders"/></h1>
          </c:otherwise>
        </c:choose>
      </div>
      <form action="/controller" method="post" align="center">
        <input type="hidden" name="command" value="TO_PAGE">
        <input type="hidden" name="forwardPage" value="jsp/client/client_menu.jsp">
        <input type="submit" class="sbmt-btn" value="<fmt:message key="back"/>">
      </form>
    </div>
  </div>
</div>
<tag:footerLayout isFixed="false"/>
</body>
</html>