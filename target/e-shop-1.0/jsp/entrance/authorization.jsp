<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>
<!DOCTYPE html>
<html>
<head>
	<title>Authorization</title>
	<%@include file="../script.jsp" %>
</head>
<body>
<div class="header-nav">
	<div class="container">
		<div class="logo wow fadeInLeft" data-wow-delay="0.5s">
			<form action="/controller" method="post">
				<input type="image" src="../../jsp/images/logo2.png" onclick="submit()">
				<input type="hidden" name="command" value="TO_PAGE">
				<input type="hidden" name="forwardPage" value="jsp/entrance/authorization.jsp">
			</form>
		</div>

		<div class="logo-right">
			<span class="menu"><img src="../images/menu.png" alt=" "/></span>
			<ul class="nav1">
				<li>
					<form action="/controller" method="post">
						<input type="hidden" name="command" value="CHANGE_LOCALE">
						<input type="hidden" name="forwardPage" value="jsp/entrance/authorization.jsp">
						<select name="locale" onchange="submit()">
							<option value="en" ${sessionScope.locale == 'en' ? 'selected' : ''}><fmt:message key="en"/></option>
							<option value="ru" ${sessionScope.locale == 'ru' ? 'selected' : ''}><fmt:message key="rus"/></option>
						</select>
					</form>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="login-form wow fadeInUp" data-wow-delay="0.5s">
	<div class="container">
		<p align="center">${message}</p>
		<div class="login-form-info">
			<div class="login-header">
				<div class="login-header-left">
					<h3><fmt:message key="login"/> </h3>
				</div>
			</div>
			<form action="/controller" align="center" method="post">
				<input type="text" class="men" name="login" placeholder="<fmt:message key="Username"/> " required/>
				<input type="password" class="lock" name="password" placeholder="<fmt:message key="password"/>" required/>
				<input type="hidden" name="command" value="AUTHORIZATION"/>
				<input type="submit" value="<fmt:message key="sign_in"/> ">
			</form>
		</div>
		<div class="new-people">
			<p><fmt:message key="supply_for_registration"/></p>
			<div class="reg">
				<form action="/controller" method="post">
					<input type="submit" value="<fmt:message key="registration"/>">
					<input type="hidden" name="command" value="TO_PAGE">
					<input type="hidden" name="forwardPage" value="jsp/entrance/registration.jsp">
				</form>
			</div>
		</div>
	</div>
</div>
<tag:footerLayout isFixed="true"/>
</body>
</html>