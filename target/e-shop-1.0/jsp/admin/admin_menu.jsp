<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>
<!DOCTYPE html>
<html>
<head>
  <title>e-Shop</title>
  <%@include file="../script.jsp" %>
</head>
<body>
<%@include file="../header.jsp"%>

<div class="header-info wow fadeInUp" data-wow-delay="0.5s">
  <div class="container">
    <h3><fmt:message key="admin_page"/></h3>
  </div>
</div>

<tag:footerLayout isFixed="true"/>
</body>
</html>