<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>
<html>
<head>
  <title>Checking users</title>
  <%@include file="../script.jsp" %>
</head>

<body>

<%@include file="../header.jsp"%>

<div class="single">
  <div class="container">
    <div class="bottom">
      <h1 align="center"><fmt:message key="check_users"/></h1>
        <div id="main">
          <table id="tablesorter" class="tablesorter" border="0" cellpadding="0" cellspacing="1">
            <thead>
            <tr bgcolor="#F5F5F5">
              <th rowspan="2"><fmt:message key="Username"/></th>
              <th rowspan="2"><fmt:message key="blacklist"/></th>
              <th rowspan="2"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="user" items="${ userList }">
              <tr>
                <form action="/controller" method="post">

                  <td>
                      ${user.login}
                  </td>
                  <td>
                      ${user.blacklist}
                  </td>
                  <c:choose>
                    <c:when test="${user.blacklist == false}">
                      <td>
                        <input type="hidden" name="command" value="IN_BLACKLIST">
                        <input type="hidden" name="idUser" value="${user.userID}">
                        <input type="submit" class="sbmt-btn" value="<fmt:message key="blacklist_in"/>">
                      </td>
                    </c:when>
                    <c:when test="${user.blacklist == true}">
                      <td>
                        <input type="hidden" name="command" value="OUT_BLACKLIST">
                        <input type="hidden" name="idUser" value="${user.userID}">
                        <input type="submit" class="sbmt-btn" value="<fmt:message key="blacklist_out"/>">
                      </td>
                    </c:when>
                  </c:choose>
                </form>
              </tr>
            </c:forEach>
            </tbody>
          </table>
        </div>
      <form action="/controller" method="post" align="center">
          <input type="hidden" name="command" value="TO_PAGE">
          <input type="hidden" name="forwardPage" value="jsp/admin/admin_menu.jsp">
          <input type="submit" class="sbmt-btn" value="<fmt:message key="back"/>">
      </form>
    </div>
    <div class="clearfix"> </div>
  </div>
</div>

<tag:footerLayout isFixed="false"/>

</body>
</html>