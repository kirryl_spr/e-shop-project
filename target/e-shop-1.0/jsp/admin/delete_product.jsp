<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="Resource"/>

<html>
<head>
  <title>Delete car</title>
  <%@include file="../script.jsp" %>
</head>

<body>

<%@include file="../header.jsp"%>

<div class="single">
  <div class="container">
    <div class="bottom">
      <h1 align="center"><fmt:message key="delete_products"/></h1>
      <div id="main">
        <table id="tablesorter" class="tablesorter" border="0" cellpadding="0" cellspacing="1">
          <thead>
          <tr bgcolor="#F5F5F5">
            <th rowspan="2"><fmt:message key="idProduct"/></th>
            <th rowspan="2"><fmt:message key="name"/></th>
            <th rowspan="2"><fmt:message key="price"/></th>
            <th rowspan="2"><fmt:message key="amount"/></th>
            <th rowspan="2"><fmt:message key="category"/></th>
            <th rowspan="2"></th>
          </tr>
          </thead>
          <tbody>
          <c:forEach var="product" items="${ productList }">
            <tr>
              <form action="/controller" method="post">
                <td>
                  <input type="hidden" name="idProduct" value="${product.idProduct}">${product.idProduct}
                </td>
                <td>
                    ${product.name}
                </td>
                <td>
                    ${product.price}
                </td>
                <td>
                    ${product.amount}
                </td>
                <td>
                    ${product.category}
                </td>
                <td>
                  <input type="hidden" name="command" value="DELETE_PRODUCT">
                  <input type="submit" class="sbmt-btn" value="<fmt:message key="delete_btn"/>">
                </td>
              </form>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
      <form action="/controller" method="post" align="center">
        <input type="hidden" name="command" value="TO_PAGE">
        <input type="hidden" name="forwardPage" value="jsp/admin/admin_menu.jsp">
        <input type="submit" class="sbmt-btn" value="<fmt:message key="back"/>">
      </form>
    </div>
  </div>
</div>

<tag:footerLayout isFixed="false"/>

</body>
</html>
